from collections import Counter
import numpy as np
import pandas as pd
import re



def get_grande_annotation(df):
    ''' filtre sur la source pour ne garder que les données vues par la grande annotation
    '''
    return df.loc[(~df.Categories.isin(['[nan]', '[nan, nan]']) & (df.source == "GD")), :]

def subfinder(mylist, pattern):
    '''find a pattern (sublist) into a list 
    '''
    pattern = set(pattern)
    return set([x for x in mylist if x in pattern]) == set(pattern)

def add_categories_label(df, cats, col_name = None):
    '''add a boolean column with true if cats are in categories of the response 
    input:
        df, original dataframe with [reponse, Categories, questionIndex, authorId, source]
        cats, list of categories in 
        col_name, name of the added label column
    output:
        df, original df with a column added for the categorie boolean
    '''
    if col_name is None:
        col_name = 'cat_' + re.sub(' ', '_', str(cats[0]).lower())
    df[col_name] = df.Categories.map(lambda x: subfinder(eval(x), cats))
    return df

def category_counter(df, verbose = True):
    '''Get all categories and count the number of occurence in the dataset
    '''
    categories_ind = df.Categories.map(lambda x: eval(x))
    # Get a unique list
    all_categories = []
    all_categories = [l_item for l in categories_ind for l_item in l]
    cat_counter = Counter(all_categories) 
    if verbose:
        print("Nombre de tags différents dans la grande annotation: {}".format(len(set(cat_counter.keys()))))
    return cat_counter