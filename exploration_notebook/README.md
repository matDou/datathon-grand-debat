# Several notebook to explore and preprocess the data

+ [exploration.ipynb](exploration_notebook/exploration.ipynb), create a `all_answers` dataframe from the original `Data.zip`. Harmonization of the different sources in a formated dataframe: 

| reponse                                          	| Categories 	| questionIndex     	| authorId 	| source 	|
|--------------------------------------------------	|------------	|-------------------	|----------	|--------	|
| Dites leur la vérité. Si une situation est jug.. 	| [nan]      	| QUXVlc3Rpb246MTE2 	| 17943    	| ELF    	|

+ [prepare_train_corpus.ipynb](exploration_notebook/prepare_train_corpus.ipynb),  Create a  corpus for bert pretraining in the context of the **Grand Débat**. Concatenation of all texts from the `reponse` column from the `all_answers` dataframe produced by [exploration.ipynb](exploration_notebook/exploration.ipynb) (one answer per line).

+ [select_grande_annotation.ipynb](exploration_notebook/select_grande_annotation.ipynb), create training data for binary classification (label "Idée marquée" by default, another label could be chosen simply by changing the `INTERESSANTE` variable) from the `all_answers` dataframe produced by [exploration.ipynb](exploration_notebook/exploration.ipynb):

| reponse   	| qid                 	| label 	|
|-----------	|---------------------	|-------	|
| le peuple 	| [QUXVlc3Rpb246MTA3] 	| False 	|


