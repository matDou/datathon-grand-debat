# Classification des contributions
Ce sous-dossier contient le code permettant d'utiliser le modèle BERT afin de prédire le tag "marquée" d'une contribution. 

## Entraînement du classifieur
Le fichier principal est `run_classifier.py` extrait [du code de Thomas Wolf](https://github.com/huggingface/pytorch-pretrained-BERT).
L'entrainement peut être lancé avec la commande suivante, après avoir specifié les variables d'environnement `DATA_DIR` contenant les données et `EXP_NAME` pour créer le dossier de résultats :

```python
python run_classifier.py \
    --do_train \
    --do_eval \
    --data_dir $DATA_DIR/ \
    --distribution balanced \
    --bert_model bert-base-multilingual-cased \
    --max_seq_length 512 \
    --train_batch_size 16 \
    --learning_rate 2e-5 \
    --num_train_epochs 3.0 \
    --output_dir $DATA_DIR/output/$EXP_NAME \
    --metric_averaging binary
```

En particulier, les arguments `--distribution` et `--metric_averaging` sont nouveaux.
Le premier permet de choisir d'entraîner avec des données respectant la distribution _réelle_ de True/False, ou bien avec des données artificiellement rééquilibrées. 
Le second permet de choisir le type de moyennage effectué par [scikit-learn pour le calcul des métriques](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.recall_score.html).

## Pré-entraînement par language model
Afin d'améliorer les performances du modèle il est possible de le pré-entraîner sur le jeu de données particulier du Grand Débat via une tâche de _language modeling_. 
Cette tâche est réalisée dans `pretrain.py` extrait également [du code de Thomas Wolf](https://github.com/huggingface/pytorch-pretrained-BERT).

_Pour l'instant cette fonctionnalité n'a pas été exploitée_.

## Création des jeux de données

Les données sont tirées du Grand Débat national et de la Grande Annotation. Elles sont d'abord traitées par les notebooks dans `../exploration_notebooks` pour en retenir un fichier contenant toutes les contributions annotées au format `id, texte, question_id, label`. Finalement, les jeux utilisés par notre algorithme et séparés en _train / val / test_ sont générés par `utils/create_data.py`.

Une option importante est `--balanced`. Celle-ci permet, en l'activant, de creer un jeu d'entrainement equilibre, i.e. contenant autant de labels positifs et negatifs. Les jeux de validation et de test conservent la "vraie" repartition tres desequilibree en faveur des False.

```python
python utils/create_data.py path/to/input/file.csv path/to/output/directory --balanced
```

## TODO

- [ ] créer dynamiquement les splits pendant l'entrainement
- [ ] réorganiser `pretrain.py` pour utiliser les memes utils que la classification

