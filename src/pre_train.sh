#!/usr/bin/env bash
export DATA_DIR=/net/big/guiguetv/datathon
#export DATA_DIR = data
#export EXP_NAME = debug
python pretrain.py \
  --bert_model bert-base-multilingual-cased \
  --do_train \
  --train_file $DATA_DIR/pretrain_corrige.txt \
  --output_dir $DATA_DIR/lmft/ \
  --num_train_epochs 1.0 \
  --learning_rate 3e-5 \
  --train_batch_size 16 \
  --max_seq_length 128 \
  --gradient_accumulation_steps 2 \
  --fp16 
