import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('input_file')
parser.add_argument('output_dir')
parser.add_argument('--balanced', action='store_true', help='whether to balance the train set')
parser.add_argument('--seed', default=2610)
parser.add_argument('--val_prop', default=0.2)
parser.add_argument('--test_prop', default=0.5)
args = parser.parse_args()

np.random.seed(args.seed)

data = pd.read_csv(args.input_file, header=None, names=('_id', 'text', 'qid', 'label'))

true = data[data.label==True]
false = data[data.label==False]

print(f'There are {len(true)} True labels')
print(f'There are {len(false)} False labels')
frac_true = len(true) / len(false)

def create_split(data, prop):
    test_idx = np.random.choice(data.index, size=round(len(data)*prop), replace=False)
    test_set = data.loc[test_idx]
    return test_set, test_idx

def test_no_intersection(idx1, idx2):
    assert set(idx1).intersection(set(idx2)) == set()

def replace_labels(data):
    # replace labels by "0" and "1" for torch
    data.loc[data.label==True, 'label'] = "1"
    data.loc[data.label==False, 'label'] = "0"
    return data

# create a test set from test_prop% of the data, with the same class proportion
test_set, test_idx = create_split(data, args.test_prop)
n_test = len(test_set)

train_idx = np.setdiff1d(data.index,test_idx)
data = data.loc[train_idx]
test_no_intersection(data.index, test_set.index)

# create train / val splits
# we have 2 options:
# - either we keep the same proportion oftrue/false
# - or we balance the train set to ease training
val_set, val_idx = create_split(data, args.val_prop)
n_val = len(val_set)

train_idx = np.setdiff1d(data.index,val_idx)
train_set = data.loc[train_idx]

if args.balanced:
    # dans ce cas on reduit le train pour avoir autant de True et de False
    train_true_idx = train_set[train_set.label].index
    train_false_idx = np.random.choice(train_set[~train_set.label].index, 
            size=len(train_true_idx), replace=False)
    train_set = train_set.loc[train_true_idx.tolist()+train_false_idx.tolist()]
n_train = len(train_set)
test_no_intersection(train_set.index, val_set.index)

print('-'*10)
print('Number of train samples',train_set.shape)
print('Number of val samples',val_set.shape)
print('Number of test samples', test_set.shape)
print('-'*10)
print('There are {:.2f} % of True labels in train set'.format(len(train_set[train_set.label])/n_train * 100))
print('There are {:.2f} % of True labels in val set'.format(len(val_set[val_set.label])/n_val * 100))
print('There are {:.2f} % of True labels in test set'.format(len(test_set[test_set.label])/n_test * 100))
print('-'*10)

# it's better to replace True/False labels by 0/1.
print('Saving...')
train_set = replace_labels(train_set)
val_set = replace_labels(val_set)
test_set = replace_labels(test_set)

train_set.to_csv(args.output_dir+'/train.csv',header=True, index=False)
val_set.to_csv(args.output_dir+'/dev.csv', header=True, index=False)
test_set.to_csv(args.output_dir+'/test.csv',header=True, index=False)
