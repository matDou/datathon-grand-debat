import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('data_dir')
parser.add_argument('--seed', default=2610)
args = parser.parse_args()

np.random.seed(args.seed)

def sample(data, size):
    index = np.random.choice(data.index, size=size, replace=False)
    return data.loc[index]

train = pd.read_csv(args.data_dir+'/train.csv')
dev = pd.read_csv(args.data_dir+'/dev.csv')
test = pd.read_csv(args.data_dir+'/test.csv')

sample_train = sample(train, size=100)
sample_dev = sample(dev, size=20)
sample_test = sample(test, size=100)

sample_train.to_csv(args.data_dir+'/sample_train.csv', index=False)
sample_dev.to_csv(args.data_dir+'/sample_dev.csv', index=False)
sample_test.to_csv(args.data_dir+'/sample_test.csv', index=False)
