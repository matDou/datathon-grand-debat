# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors and The HugginFace Inc. team.
# Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""BERT finetuning runner."""

from __future__ import absolute_import, division, print_function

import argparse
import csv
import logging
import os
import random
import sys

import numpy as np
import torch
from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler,
                              TensorDataset)
from torch.utils.data.distributed import DistributedSampler
from tqdm import tqdm, trange

from pytorch_pretrained_bert.file_utils import PYTORCH_PRETRAINED_BERT_CACHE
from pytorch_pretrained_bert.modeling import BertForSequenceClassification, BertConfig, WEIGHTS_NAME, CONFIG_NAME
from pytorch_pretrained_bert.tokenization import BertTokenizer
from pytorch_pretrained_bert.optimization import BertAdam, warmup_linear

import json
import pandas as pd
import ipdb
from sklearn.metrics import recall_score, f1_score, precision_score
from sklearn.utils.class_weight import compute_class_weight
from torch.nn import CrossEntropyLoss
from utils.processing import InputExample, InputFeatures, DebatProcessor, convert_examples_to_features

logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(name)s -   %(message)s',
                    datefmt = '%m/%d/%Y %H:%M:%S',
                    level = logging.INFO)
logger = logging.getLogger(__name__)

def save_args(args):
    json.dump(vars(args), open(f'{args.output_dir}/args.json', 'w'))

def compute_metrics(all_labels, predictions, average='weighted'):
    f1 = f1_score(all_labels, predictions, average=average)
    recall = recall_score(all_labels, predictions, average=average)
    precision = precision_score(all_labels, predictions, average=average)

    return f1, recall, precision

def define_fp16_optim(optimizer_grouped_parameters, args):
    try:
        from apex.optimizers import FP16_Optimizer
        from apex.optimizers import FusedAdam
    except ImportError:
        raise ImportError("Please install apex from https://www.github.com/nvidia/apex to use distributed and fp16 training.")

    optimizer = FusedAdam(optimizer_grouped_parameters,
                          lr=args.learning_rate,
                          bias_correction=False,
                          max_grad_norm=1.0)
    if args.loss_scale == 0:
        optimizer = FP16_Optimizer(optimizer, dynamic_loss_scale=True)
    else:
        optimizer = FP16_Optimizer(optimizer, static_loss_scale=args.loss_scale)

    return optimizer

def features2dataset(features):
    all_ids = torch.tensor([f._id for f in features], dtype=torch.long)
    all_input_ids = torch.tensor([f.input_ids for f in features], dtype=torch.long)
    all_input_mask = torch.tensor([f.input_mask for f in features], dtype=torch.long)
    all_segment_ids = torch.tensor([f.segment_ids for f in features], dtype=torch.long)
    all_label_ids = torch.tensor([f.label_id for f in features], dtype=torch.long)
    return TensorDataset(all_ids, all_input_ids, all_input_mask, all_segment_ids, all_label_ids)

def load_model(args, num_labels=2):
    if args.pretrained_model:
        logger.info('Loading fine tuned model from {}'.format(args.pretrained_model))
        model = BertForSequenceClassification.from_pretrained(args.pretrained_model, num_labels=num_labels)
    else:
        cache_dir = args.cache_dir if args.cache_dir else os.path.join(PYTORCH_PRETRAINED_BERT_CACHE, 'distributed_{}'.format(args.local_rank))
        model = BertForSequenceClassification.from_pretrained(args.bert_model,
                  cache_dir=cache_dir,
                  num_labels = num_labels)

    return model

def accuracy(out, labels):
    outputs = np.argmax(out, axis=1)
    return np.sum(outputs == labels)

def set_args():
    parser = argparse.ArgumentParser()

    ## Required parameters
    parser.add_argument("--data_dir",
                        default=None,
                        type=str,
                        required=True,
                        help="The input data dir. Should contain the .tsv files (or other data files) for the task.")
    parser.add_argument('--distribution',
                        default='original',
                        type=str,
                        help='Subdirectory containing the data')
    parser.add_argument("--bert_model", default=None, type=str, required=True,
                        help="Bert pre-trained model selected in the list: bert-base-uncased, "
                        "bert-large-uncased, bert-base-cased, bert-large-cased, bert-base-multilingual-uncased, "
                        "bert-base-multilingual-cased, bert-base-chinese.")
    parser.add_argument("--pretrained_model", default=None, type=str, help='Path to an already fine tuned model')
    parser.add_argument("--output_dir",
                        default=None,
                        type=str,
                        required=True,
                        help="The output directory where the model predictions and checkpoints will be written.")

    ## Other parameters
    parser.add_argument('--debug',
            action='store_true')
    parser.add_argument('--test_set',
                        action='store_true',
                        help='Whether to evaluate on the test set')
    parser.add_argument('--no_fine_tune',
            action='store_true',
            help='Set if you only want to train the classification layer')
    parser.add_argument("--cache_dir",
                        default="",
                        type=str,
                        help="Where do you want to store the pre-trained models downloaded from s3")
    parser.add_argument("--max_seq_length",
                        default=128,
                        type=int,
                        help="The maximum total input sequence length after WordPiece tokenization. \n"
                             "Sequences longer than this will be truncated, and sequences shorter \n"
                             "than this will be padded.")
    parser.add_argument("--do_train",
                        action='store_true',
                        help="Whether to run training.")
    parser.add_argument("--do_eval",
                        action='store_true',
                        help="Whether to run eval on the dev set.")
    parser.add_argument("--do_lower_case",
                        action='store_true',
                        help="Set this flag if you are using an uncased model.")
    parser.add_argument("--train_batch_size",
                        default=32,
                        type=int,
                        help="Total batch size for training.")
    parser.add_argument("--eval_batch_size",
                        default=8,
                        type=int,
                        help="Total batch size for eval.")
    parser.add_argument('--metric_averaging',
                        default='binary',
                        type=str,
                        help='Method to use for sklearn calculation of f1/recall/precision\n'
                             'Can be [binary, micro, macro, weighted, None]')
    parser.add_argument("--learning_rate",
                        default=5e-5,
                        type=float,
                        help="The initial learning rate for Adam.")
    parser.add_argument("--num_train_epochs",
                        default=3.0,
                        type=float,
                        help="Total number of training epochs to perform.")
    parser.add_argument("--warmup_proportion",
                        default=0.1,
                        type=float,
                        help="Proportion of training to perform linear learning rate warmup for. "
                             "E.g., 0.1 = 10%% of training.")
    parser.add_argument("--no_cuda",
                        action='store_true',
                        help="Whether not to use CUDA when available")
    parser.add_argument("--local_rank",
                        type=int,
                        default=-1,
                        help="local_rank for distributed training on gpus")
    parser.add_argument('--seed',
                        type=int,
                        default=42,
                        help="random seed for initialization")
    parser.add_argument('--gradient_accumulation_steps',
                        type=int,
                        default=1,
                        help="Number of updates steps to accumulate before performing a backward/update pass.")
    parser.add_argument('--fp16',
                        action='store_true',
                        help="Whether to use 16-bit float precision instead of 32-bit")
    parser.add_argument('--loss_scale',
                        type=float, default=0,
                        help="Loss scaling to improve fp16 numeric stability. Only used when fp16 set to True.\n"
                             "0 (default value): dynamic loss scaling.\n"
                             "Positive power of 2: static loss scaling value.\n")
    args = parser.parse_args()
    return args

def set_backend(args):
    if args.local_rank == -1 or args.no_cuda:
        device = torch.device("cuda" if torch.cuda.is_available() and not args.no_cuda else "cpu")
        n_gpu = torch.cuda.device_count()
    else:
        torch.cuda.set_device(args.local_rank)
        device = torch.device("cuda", args.local_rank)
        n_gpu = 1
        # Initializes the distributed backend which will take care of sychronizing nodes/GPUs
        torch.distributed.init_process_group(backend='nccl')

    return device, n_gpu

def plant_seeds(seed, n_gpu):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if n_gpu > 0:
        torch.cuda.manual_seed_all(seed)

def count_parameters(model):
    trainable_params = 0
    for param in model.parameters():
        if param.requires_grad:
            trainable_params += 1
    logger.info('TRAINABLE PARAMETERS: {}'.format(trainable_params))

def send_model_to_device(model, device, n_gpu, args):
    if args.fp16:
        model.half()
    model.to(device)
    if args.local_rank != -1:
        try:
            from apex.parallel import DistributedDataParallel as DDP
        except ImportError:
            raise ImportError("Please install apex from https://www.github.com/nvidia/apex to use distributed and fp16 training.")

        model = DDP(model)
    elif n_gpu > 1:
        model = torch.nn.DataParallel(model)
    return model

def main(args):
    device, n_gpu = set_backend(args)
    logger.info("device: {} n_gpu: {}, distributed training: {}, 16-bits training: {}".format(
        device, n_gpu, bool(args.local_rank != -1), args.fp16))

    if args.gradient_accumulation_steps < 1:
        raise ValueError("Invalid gradient_accumulation_steps parameter: {}, should be >= 1".format(
                            args.gradient_accumulation_steps))

    args.train_batch_size = args.train_batch_size // args.gradient_accumulation_steps

    if not args.do_train and not args.do_eval:
        raise ValueError("At least one of `do_train` or `do_eval` must be True.")

    if os.path.exists(args.output_dir) and os.listdir(args.output_dir) and args.do_train:
        raise ValueError("Output directory ({}) already exists and is not empty.".format(args.output_dir))
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    # save args to reproduce exp
    save_args(args)
    plant_seeds(args.seed, n_gpu)

    processor = DebatProcessor()
    label_list = processor.get_labels()
    num_labels = len(label_list)

    tokenizer = BertTokenizer.from_pretrained(args.bert_model, do_lower_case=args.do_lower_case)

    train_examples = None
    num_train_optimization_steps = None
    if args.do_train:
        train_dir = args.data_dir+'/'+args.distribution
        train_examples = processor.get_train_examples(train_dir, args.debug)
        num_train_optimization_steps = int(
            len(train_examples) / args.train_batch_size / args.gradient_accumulation_steps) * args.num_train_epochs
        if args.local_rank != -1:
            num_train_optimization_steps = num_train_optimization_steps // torch.distributed.get_world_size()

    # Prepare model
    model = load_model(args, num_labels)
    
    # freeze all layers except dropout and classification
    if args.no_fine_tune:
        logger.info('Freezing pre-trained layers')
        for param in model.bert.parameters():
            param.requires_grad = False

    count_parameters(model)

    model = send_model_to_device(model, device, n_gpu, args)

    # Prepare optimizer
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
        ]
    if args.fp16:
        optimizer = define_fp16_optim(optimizer_grouped_parameters, args)

    else:
        optimizer = BertAdam(optimizer_grouped_parameters,
                             lr=args.learning_rate,
                             warmup=args.warmup_proportion,
                             t_total=num_train_optimization_steps)

    # Train loop
    global_step = 0
    nb_tr_steps = 0
    tr_loss = 0
    if args.do_train:
        train_features , too_long_seq = convert_examples_to_features(
            train_examples, label_list, args.max_seq_length, tokenizer)
        logger.info(f'{too_long_seq} SEQUENCES WERE TRUNCATED')
        logger.info("***** Running training *****")
        logger.info("  Num examples = %d", len(train_examples))
        logger.info("  Batch size = %d", args.train_batch_size)
        logger.info("  Num steps = %d", num_train_optimization_steps)
        train_data = features2dataset(train_features)
        if args.local_rank == -1:
            train_sampler = RandomSampler(train_data)
        else:
            train_sampler = DistributedSampler(train_data)
        train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=args.train_batch_size)

        # compute class weights with formula from
        # https://github.com/scikit-learn/scikit-learn/blob/3b35104c93cb53f67fb5f52ae2fece76ef7144da/sklearn/utils/class_weight.py#L8
        n_samples = len(train_examples)
        y = [example.label for example in train_examples]
        class_weights = compute_class_weight('balanced', label_list, y)
        class_weights = torch.Tensor(class_weights).to(device)
        loss_fct = CrossEntropyLoss(weight=class_weights)
        for label, weight in zip(label_list, class_weights):
            logger.info('Weight for class {}: {:.2f}'.format(label, weight))

        model.train()
        for _ in trange(int(args.num_train_epochs), desc="Epoch"):
            predictions = []
            all_labels = []
 
            tr_loss = 0
            nb_tr_examples, nb_tr_steps = 0, 0
            for step, batch in enumerate(tqdm(train_dataloader, desc="Iteration")):
                batch = tuple(t.to(device) for t in batch)
                _ids, input_ids, input_mask, segment_ids, label_ids = batch
                # loss = model(input_ids, segment_ids, input_mask, label_ids)
                logits = model(input_ids, segment_ids, input_mask)# , label_ids)
                loss = loss_fct(logits.view(-1, num_labels), label_ids.view(-1))
                if n_gpu > 1:
                    loss = loss.mean() # mean() to average on multi-gpu.
                if args.gradient_accumulation_steps > 1:
                    loss = loss / args.gradient_accumulation_steps

                if args.fp16:
                    optimizer.backward(loss)
                else:
                    loss.backward()

                # store predictions to monitor metric
                predictions.append(logits.detach().cpu().numpy())
                all_labels.append(label_ids.detach().cpu().numpy())

                tr_loss += loss.item()
                nb_tr_examples += input_ids.size(0)
                nb_tr_steps += 1
                if (step + 1) % args.gradient_accumulation_steps == 0:
                    if args.fp16:
                        # modify learning rate with special warm up BERT uses
                        # if args.fp16 is False, BertAdam is used that handles this automatically
                        lr_this_step = args.learning_rate * warmup_linear(global_step/num_train_optimization_steps, args.warmup_proportion)
                        for param_group in optimizer.param_groups:
                            param_group['lr'] = lr_this_step
                    optimizer.step()
                    optimizer.zero_grad()
                    global_step += 1

            # compute recall each epoch
            all_labels = np.hstack(all_labels)
            predictions = np.vstack(predictions)
            preds = np.argmax(predictions, axis=1)

            train_f1, train_recall, train_precision = compute_metrics(all_labels, preds, 
                    average=args.metric_averaging)
            logger.info('F1: {:.3f}'.format(train_f1))
            logger.info('Recall: {:.3f}'.format(train_recall))
            logger.info('Precision: {:.3f}'.format(train_precision))

    if args.do_train:
        # Save a trained model and the associated configuration
        model_to_save = model.module if hasattr(model, 'module') else model  # Only save the model it-self
        output_model_file = os.path.join(args.output_dir, WEIGHTS_NAME)
        torch.save(model_to_save.state_dict(), output_model_file)
        output_config_file = os.path.join(args.output_dir, CONFIG_NAME)
        with open(output_config_file, 'w') as f:
            f.write(model_to_save.config.to_json_string())

        # Load a trained model and config that you have fine-tuned
        config = BertConfig(output_config_file)
        model = BertForSequenceClassification(config, num_labels=num_labels)
        model.load_state_dict(torch.load(output_model_file))
    else:
        model = load_model(args, num_labels)
    model.to(device)

    if args.do_eval and (args.local_rank == -1 or torch.distributed.get_rank() == 0):
        val_dir = args.data_dir+'/'+args.distribution
        eval_examples = processor.get_dev_examples(val_dir, args.test_set, args.debug)
        eval_features, too_long_seq = convert_examples_to_features(
            eval_examples, label_list, args.max_seq_length, tokenizer)
        logger.info(f'{too_long_seq} SEQUENCES WERE TRUNCATED')
        logger.info("***** Running evaluation *****")
        logger.info("  Num examples = %d", len(eval_examples))
        logger.info("  Batch size = %d", args.eval_batch_size)
        eval_data = features2dataset(eval_features)
        # Run prediction for full data
        # TODO: etre sur qu'on a les donnees dans le bon ordre
        eval_sampler = SequentialSampler(eval_data)
        eval_dataloader = DataLoader(eval_data, sampler=eval_sampler, batch_size=args.eval_batch_size)

        model.eval()
        eval_loss, eval_accuracy = 0, 0
        nb_eval_steps, nb_eval_examples = 0, 0

        all_ids = []
        predictions = []
        all_labels = []
 
        for _ids, input_ids, input_mask, segment_ids, label_ids in tqdm(eval_dataloader, desc="Evaluating"):
            input_ids = input_ids.to(device)
            input_mask = input_mask.to(device)
            segment_ids = segment_ids.to(device)
            label_ids = label_ids.to(device)

            with torch.no_grad():
                tmp_eval_loss = model(input_ids, segment_ids, input_mask, label_ids)
                logits = model(input_ids, segment_ids, input_mask)

            logits = logits.detach().cpu().numpy()
            label_ids = label_ids.to('cpu').numpy()
            tmp_eval_accuracy = accuracy(logits, label_ids)

            all_ids.append(_ids)
            predictions.append(logits)
            all_labels.append(label_ids)

            eval_loss += tmp_eval_loss.mean().item()
            eval_accuracy += tmp_eval_accuracy

            nb_eval_examples += input_ids.size(0)
            nb_eval_steps += 1

        # compute metrics on eval set
        all_ids = np.hstack(all_ids)
        all_labels = np.hstack(all_labels)
        predictions = np.vstack(predictions)
        predictions = np.argmax(predictions, axis=1)

        f1, recall, precision = compute_metrics(all_labels, predictions,
                average=args.metric_averaging)

        eval_loss = eval_loss / nb_eval_steps
        eval_accuracy = eval_accuracy / nb_eval_examples
        loss = tr_loss/nb_tr_steps if args.do_train else None
        result = {'eval_loss': eval_loss,
                  'eval_recall': recall,
                  'eval_precision': precision,
                  'eval_f1': f1,
                  'eval_accuracy': eval_accuracy,
                  'global_step': global_step,
                  'loss': loss}
        if args.do_train:
            result['train_recall'] = train_recall

        if args.test_set:
            output_eval_file = os.path.join(args.output_dir, "test_results.txt")
            output_prediction_file = os.path.join(args.output_dir, "test_predictions.csv")
        else:
            output_eval_file = os.path.join(args.output_dir, "eval_results.txt")
            output_prediction_file = os.path.join(args.output_dir, "eval_predictions.csv")

        with open(output_eval_file, "w") as writer:
            logger.info("***** Eval results *****")
            for key in sorted(result.keys()):
                logger.info("  %s = %s", key, str(result[key]))
                writer.write("%s = %s\n" % (key, str(result[key])))

        # last check si jamais les metrics sont mal calculees
        ipdb.set_trace()
        
        # write predictions
        data = np.vstack([all_ids, all_labels, predictions]).transpose() 
        predictions = pd.DataFrame(data,
            columns=('_id', 'label', 'prediction'))
        predictions.to_csv(output_prediction_file, index=False)

if __name__ == "__main__":
    args = set_args()
    main(args)
