export DATA_DIR=/net/big/guiguetv/datathon/
export EXP_NAME=debug

python run_classifier.py \
    --debug \
    --do_eval \
    --test_set \
    --pretrained_model $DATA_DIR/output/$EXP_NAME/model.tar.gz \
    --data_dir $DATA_DIR/ \
    --distribution balanced \
    --bert_model bert-base-multilingual-cased \
    --max_seq_length 512 \
    --train_batch_size 6 \
    --learning_rate 2e-5 \
    --num_train_epochs 1.0 \
    --output_dir $DATA_DIR/output/$EXP_NAME \
    --gradient_accumulation_steps 2 \
    --metric_averaging binary
