export DATA_DIR=/net/big/guiguetv/datathon/
export EXP_NAME=balanced.3epochs.20190326

python run_classifier.py \
    --pretrained_model $DATA_DIR/output/$EXP_NAME/model.tar.gz \
    --do_eval \
    --test_set \
    --data_dir $DATA_DIR/ \
    --distribution balanced \
    --bert_model bert-base-multilingual-cased \
    --max_seq_length 512 \
    --output_dir $DATA_DIR/output/$EXP_NAME \
